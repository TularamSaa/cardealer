function compare(a, b) {
  if (a.car_model < b.car_model) {
    return -1;
  }
  if (a.car_model > b.car_model) {
    return 1;
  }
  return 0;
}
//

let sortedCarModels = function (inventory) {
  const sortedInventoryCarModel = inventory.sort(compare);
  for (let i = 0; i < sortedInventoryCarModel.length; i++) {
    sortedInventoryCarModel[i] = sortedInventoryCarModel[i].car_model;
  }
  return sortedInventoryCarModel;
};

module.exports = sortedCarModels;
