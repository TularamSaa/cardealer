
let carByYears = function(inventory) {
  let length = inventory.length
  let array1 = []
  for (let c=0; c<length; c++) {
      let eachCar = inventory[c];
      array1.push(eachCar.car_year);
  }
  return array1;
}

module.exports = carByYears
  