function findCarbyId(inventory, id) {
  if (!inventory || !id || id > inventory.length) {
    return "Invalid Input";
  } else {
    for (let i = 0; i < inventory.length; i++) {
      let result = inventory[i];
      if (result.id === id) {
        return `Car ${result.id} is a ${result.car_year} ${result.car_make} ${result.car_model}`;
      }
    }
  }
}
module.exports = findCarbyId;
