function problem6(inventory) {
    let BMWAndAudi = [];
    let j = 0;
    for (let i = 0; i < inventory.length; i++) {
      if (inventory[i].car_make === "Audi" || inventory[i].car_make === "BMW") {
        BMWAndAudi.push(inventory[i]);
        BMWAndAudi[j] = JSON.stringify(BMWAndAudi[j]);
        j++;
      }
    }
    return BMWAndAudi;
  }
  
  module.exports = problem6;
  