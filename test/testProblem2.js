let inventory = require("../input");
let problem2 = require("../problem2");

let result = problem2(inventory);
console.log(`Last car is a ${result.car_make} ${result.car_model}`);
