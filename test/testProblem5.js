let inventory = require("../input");
let problem4 = require("../problem4");
let testModule = require("../problem5");

let years = problem4(inventory);

let carsOlderThan2000 = testModule(inventory, years);
console.log(carsOlderThan2000);
console.log(
  `The numbers of car older than the year 2000 is, ${carsOlderThan2000.length}`
);
