// const inventory = require("./input");

function carsOlderThan2000(inventory, years) {
  let oldCar = [];
  for (let i = 0; i < inventory.length; i++) {
    if (inventory[i].car_year < 2000) {
      oldCar.push(inventory[i]);
    }
  }
  return oldCar;
}
module.exports = carsOlderThan2000;
